# CS 1440 Assignment 0: Caesar Cipher


* [Instructions](doc/Instructions.md)
* [Expected Output](doc/Expected_Output.md)
* [Hints](doc/Hints.md)
* [Rubric](doc/Rubric.md)


## NOTE
This was an assignment done for CS1440 at Utah State University, much of the project was done by Erik Fowler, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code and some of the doc files. To run this program, run the caesar.py file. Then to process a file, enter one of the file names contained in the data/ folder, followed by how many rotations you'd like to do to "solve" the code. If you would like to try all rotations, enter "all". Then hit "e" to exit when finished.


## Overview

As your first official coding project at DuckieCorp, you are to write a program to encode/decode sensitive messages using the
 Caesar cipher. Using the Caesar cipher was the customer's idea (the account representative didn't have the heart to point out
 how bad of an idea this is). At any rate, DuckieCorp project management has decided that this is the perfect project for you
 to get your feet wet.


## Objectives

-   Become familiar with the command line interface
-   Learn the basics of the git version control system
    - Understand the benefits of version control
    - Practice correctly submitting an assignment to GitLab
-   Becoming familiar with your IDE
-   Review Python language concepts
    -   Arithmetic & logic
    -   Loops & conditions
    -   String operations
    -   Responding to user input
-   Opening and reading files in Python
