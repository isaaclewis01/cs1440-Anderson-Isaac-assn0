#!/usr/bin/python3
import os

def main():
    print("Pnrfne Pvcure!")
    print("")

    # Variable defining
    userInput = ''
    # Loop to continually take the program to main menu
    while userInput != 'e':
        userInput = ''
        # Loop to test user input for main menu
        while userInput != 'p' and userInput != 'e':
            print("P)rocess a file")
            print("E)xit")
            userInput = input("?> ").lower()
            print("")

            # If user wants to enter a file
            if userInput == 'p':
                fileInput = input("Enter the name of a file> ")
                # Test whether it's a valid file
                if os.access(fileInput, os.R_OK):
                    # Create file variable
                    file = open(fileInput, "r")
                    readFile = file.read()
                    file.close()
                    rotInput = input("Rotation distance (0-25, or 'all')> ")
                    print("")
                    finalText = ""
                    # Test if rotation input is valid
                    # If user enters "all"
                    if rotInput.lower() == "all":
                        for j in range(26):
                            finalText = ""
                            for i in range(len(readFile)):
                                if 65 <= ord(readFile[i: i + 1]) <= 90:
                                    if ord(readFile[i: i + 1]) + j > 90:
                                        finalText += chr(ord(readFile[i: i + 1]) + j - 26)
                                    else:
                                        finalText += chr(ord(readFile[i: i + 1]) + j)
                                elif 97 <= ord(readFile[i: i + 1]) <= 122:
                                    if ord(readFile[i: i + 1]) + j > 122:
                                        finalText += chr(ord(readFile[i: i + 1]) + j - 26)
                                    else:
                                        finalText += chr(ord(readFile[i: i + 1]) + j)
                                else:
                                    finalText += chr(ord(readFile[i: i + 1]))
                            print("=====================================")
                            print(fileInput + " rotated by " + str(j) + " positions.")
                            print("=====================================")
                            print(finalText)
                            print("\n")

                    # If user enters "0-25"
                    elif 0 <= eval(rotInput) <= 25:
                        print("=====================================")
                        print(fileInput + " rotated by " + rotInput + " positions.")
                        print("=====================================")
                        for i in range(len(readFile)):
                            if 65 <= ord(readFile[i: i + 1]) <= 90:
                                if ord(readFile[i: i + 1]) + eval(rotInput) > 90:
                                    finalText += chr(ord(readFile[i: i + 1]) + eval(rotInput) - 26)
                                else:
                                    finalText += chr(ord(readFile[i: i + 1]) + eval(rotInput))
                            elif 97 <= ord(readFile[i: i + 1]) <= 122:
                                if ord(readFile[i: i + 1]) + eval(rotInput) > 122:
                                    finalText += chr(ord(readFile[i: i + 1]) + eval(rotInput) - 26)
                                else:
                                    finalText += chr(ord(readFile[i: i + 1]) + eval(rotInput))
                            else:
                                finalText += chr(ord(readFile[i : i + 1]))
                        # Print final output
                        print(finalText)
                        print("\n")
                    else:
                        print("ERROR! Invalid rotation distance\n")
                else:
                    print("ERROR! This file does not exist or cannot be opened!\n")


main()